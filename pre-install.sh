#!/bin/sh

# Check if connected to internet

if ping -c4 -q 8.8.8.8; then echo "Network present"

# Update the system clock

timedatectl set-ntp true

