ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

hwclock --systohc

nano /etc/locale.gen

locale-gen

echo "en_IN UTF-8\nen_US.UTF-8 UTF-8\nen_US ISO-8859-1" > /etc/locale.conf

echo "scmh.badlapur" >> /etc/hostname

echo "127.0.1.1 scmh.badlapur.localdomain   scmh.badlapur" >> /etc/hosts

